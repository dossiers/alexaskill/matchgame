var Metalsmith  = require('metalsmith');
var markdown    = require('metalsmith-markdown');
var layouts     = require('metalsmith-layouts');
var permalinks  = require('metalsmith-permalinks');

// export class MetalsmithSiteModule {
//   constructor() {
//     console.log('MetalsmithSiteModule loaded....');
//   }
// }


Metalsmith(__dirname)
  .metadata({
    title: "Match Game - Alexa Skill",
    description: "Word match memory game. Match boxes with the same words by opening a pair of boxes at the same time.",
    producer: "Sideway Bot",
    url: "https://www.amazon.com/s/?url=search-alias%3Dalexa-skills&field-keywords=sideway+bot"
  })
  .source('../input')
  .destination('../public')
  .clean(false)
  .use(markdown())
  .use(permalinks())
  .use(layouts({
    engine: 'handlebars',
    directory: '../layouts'
  }))
  .build(function(err, files) {
    if (err) { throw err; }
  });
